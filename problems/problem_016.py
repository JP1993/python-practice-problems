# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if x > 0 and x < 11 and y > 0 and y < 11:
        return "it fits"
    else:
        return "it doesn't fit"
